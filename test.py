"""
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
import binascii

###########################GENERATE CERTIFICATE PAIR########################

new_key = RSA.generate(1024)

private_key = new_key
public_key = new_key.publickey()

################ENCRYPT/DECRYPT DATA WITH CERTIFICATE#######################

message = 'CODE EVERYDAY TO GET BETTER'

cipher = PKCS1_OAEP.new(public_key)
ciphertext = cipher.encrypt(message.encode())
print(binascii.hexlify(ciphertext).decode())
"""