import pandas as pd
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, cohen_kappa_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
import pickle
import os
import sys


# --- Constants ---

path = "./models"


# --- Utility Functions ---

def display_metrics(model_name, metrics, k):
    print('----------------------------------------------')
    print(model_name + ':')
    print('Misclassified samples: %d' % int(sum(metrics["missclassified"])/k))
    print('Accuracy: %.2f' % (sum(metrics["acc_score"])/k))
    print('Precision: %.2f' % (sum(metrics["pre_score"])/k))
    print('Recall: %.2f' % (sum(metrics["rec_score"])/k))
    print('F1_score: %.2f' % (sum(metrics["f1_score"])/k))
    print('Kappa: %.2f' % (sum(metrics["kappa_score"])/k))
    print('----------------------------------------------')


def kfold_train(model_name, model, X, y, k):

    kf = KFold(n_splits=k, random_state=None)
    metrics = {
        "missclassified": [],
        "acc_score": [],
        "pre_score": [],
        "rec_score": [],
        "f1_score": [],
        "kappa_score": []
    }
    max_kappa = 0
    index = 1

    for train_index, test_index in kf.split(X):

        sys.stdout.write('\r' + model_name + ' progression: %d/%d' % (index, k))

        X_train, X_test = X[train_index, :], X[test_index, :]
        y_train, y_test = [y[i] for i in train_index], [y[j] for j in test_index]
        sc = StandardScaler()
        sc.fit(X_train)
        X_train_std = sc.transform(X_train)
        X_test_std = sc.transform(X_test)

        model.fit(X_train_std, y_train)
        y_pred = model.predict(X_test_std)

        metrics["missclassified"].append((y_test != y_pred).sum())
        metrics["acc_score"].append(accuracy_score(y_test, y_pred))
        metrics["pre_score"].append(precision_score(y_test, y_pred, average='macro', zero_division=0))
        metrics["rec_score"].append(recall_score(y_test, y_pred, average='macro', zero_division=0))
        metrics["f1_score"].append(f1_score(y_test, y_pred, average='macro', zero_division=0))
        metrics["kappa_score"].append(cohen_kappa_score(y_test, y_pred))

        if metrics["kappa_score"][-1] >= max_kappa:
            pickle.dump(model, open(os.path.join(path, model_name + '.sav'), 'wb'))

        index += 1

    print()
    display_metrics(model_name, metrics, k)


# --- dataset loading ---

df = pd.read_csv("dataset/dataset.csv")
df = shuffle(df)

X = df.iloc[:, :13].values
y = []

attributes = df.iloc[:, 13].values

labels = ["Plaintext", "Shift", "Affine", "Bifid", "Vigenere", "Substitution", "Hill"]

for cl in attributes:
    if cl.split('_')[0] in labels:
        y.append(labels.index(cl.split('_')[0]))
    else:
        y.append(-1)


"""
print("attributes = ", attributes)
print("X = ", X)
print("y = ", y)
"""


# --- Support Machine Vector ---
from sklearn.svm import SVC

svm = SVC(kernel='linear', C=1.0, random_state=0)
kfold_train("svm", svm, X, y, 10)


# --- Decision Tree ---
from sklearn.tree import DecisionTreeClassifier

tree = DecisionTreeClassifier(criterion='entropy', max_depth=20, random_state=0)
kfold_train("tree", tree, X, y, 10)


# --- K-Nearest Neighbors ---
from sklearn.neighbors import KNeighborsClassifier

knn = KNeighborsClassifier(n_neighbors=5, p=2, metric='minkowski')
kfold_train("knn", knn, X, y, 10)


# --- Random Forest ---
from sklearn.ensemble import RandomForestClassifier

forest = RandomForestClassifier(criterion='entropy', n_estimators=1000, random_state=1)
kfold_train("forest", forest, X, y, 10)


# --- Histogram Gradient Boosting ---
from sklearn.ensemble import HistGradientBoostingClassifier

hgb = HistGradientBoostingClassifier(random_state=1, learning_rate=0.1)
kfold_train("hgb", hgb, X, y, 10)
