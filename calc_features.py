import pandas as pd
from scipy.stats import chisquare
from math import sqrt, log
import sys

#--- Constants ---

ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
ENGLISH_DISTRIBUTION = [8.4966, 2.0720, 4.5388, 3.3844, 11.1607, 1.8121, 2.4705, 3.0034, 7.5448, 0.1965, 1.1016,
                        5.4893, 3.0129, 6.6544, 7.1635, 3.1671, 0.1962, 7.5809, 5.7351, 6.9509, 3.6308, 1.0074,
                        1.2899, 0.2902, 1.7779, 0.2721]


#--- utility functions ---

def calc_frequency(text, letter):
    return text.count(letter) * 100 / len(text)


def calc_all_frequencies(text, alphabet):
    freq = []
    for letter in alphabet:
        freq.append(calc_frequency(text, letter))
    return freq


def get_bigrams(text):
    bigrams = []
    for i in range(0, len(text) - 1):
        bigrams.append(text[i] + text[i + 1])
    return bigrams


def get_unique(list):
    unique = []
    for e in list:
        if e not in unique:
            unique.append(e)
    return unique

def count_letters(text):
    letter_count = []
    for l in ALPHABET:
        count = 0
        for char in text:
            if char == l:
                count += 1
        letter_count.append(count)
    return letter_count


def euclidean_dist(x1, x2):
    return sqrt((x1 - x2) ** 2)


def calc_CHI2(instance):
    chisq, p = chisquare(f_obs=calc_all_frequencies(instance, ALPHABET), f_exp=ENGLISH_DISTRIBUTION)
    return chisq


def calc_DBL(instance):
    for i in range(0, len(instance) - 1, 2):
        if instance[i] == instance[i + 1]:
            return 1
    return 0


def calc_HASletter(instance, char):
    return char in instance


def calc_IoC(instance):
    letter_count = count_letters(instance)
    ioc = 0
    for ni in letter_count:
        ioc += ni * (ni - 1)
    return ioc / (len(instance) * (len(instance) - 1) / 26)


def calc_DIC(instance):
    bigrams = get_bigrams(instance)
    unique_bigrams = get_unique(bigrams)
    count = 0
    for bigram in unique_bigrams:
        rep = 0
        for i in range(0, len(instance) - 1):
            if bigram == instance[i] + instance[i + 1]:
                rep += 1
        if rep >= 2:
            count += 1
    return count / len(unique_bigrams)


def calc_LR(instance):
    letter_count = count_letters(instance)
    return letter_count.count(3) / len(letter_count)


def calc_MKA(instance):
    pr = []
    for i in range(1, 16):
        count = 0
        right_shift = instance[i:]
        for j in range(len(right_shift)):
            if right_shift[j] == instance[j]:
                count += 1
        pr.append(count/(len(right_shift)))
    return max(pr)


def calc_NOMOR(instance):
    freq = calc_all_frequencies(instance, ALPHABET)
    temp_freq = [(i, freq[i]) for i in range(len(freq))]
    sorted_freq = sorted(temp_freq, key=lambda x: x[1])
    dist = 0
    for j in range(len(sorted_freq)):
        dist += euclidean_dist(j, sorted_freq[j][0])
    return dist

def calc_SHAN(instance):
    freq = calc_all_frequencies(instance, ALPHABET)
    sum = 0
    for i in range(len(freq)):
        if freq[i] != 0:
            sum -= freq[i] * log(freq[i], 10)
    return sum

def calc_REP(instance):
    letter_count = count_letters(instance)
    count = 0
    total_rep = 0
    for lrep in letter_count:
        if 20 <= lrep <= 50:
            count += 1
            total_rep += lrep
    if total_rep != 0:
        return count/total_rep
    else:
        return 0


def calc_ROD(instance):
    letter_count = count_letters(instance)
    odd_letter_count = count_letters(instance[0:-1:2])
    letter_count = [letter_count[i] - 1 if letter_count[i] >= 0 else 0 for i in range(len(letter_count))]
    return sum(odd_letter_count) / sum(letter_count)


#--- instances loading ---

input_df = pd.read_csv("./dataset/processed_ciphertext.csv")
labels = ["Plaintext", "Shift", "Affine", "Bifid", "Vigenere", "Substitution", "Hill"]

def calc_features(row, label):
    features_dict = {}
    features_dict["CHI2"] = calc_CHI2(row[label])
    features_dict["DBL"] = calc_DBL(row[label])
    features_dict["HAS_J"] = calc_HASletter(row[label], "J")
    features_dict["HAS_H"] = calc_HASletter(row[label], "H")
    features_dict["HAS_X"] = calc_HASletter(row[label], "X")
    features_dict["IoC"] = calc_IoC(row[label])
    features_dict["DIC"] = calc_DIC(row[label])
    features_dict["LR"] = calc_LR(row[label])
    features_dict["MKA"] = calc_MKA(row[label])
    features_dict["NOMOR"] = calc_NOMOR(row[label])
    features_dict["SHAN"] = calc_SHAN(row[label])
    features_dict["REP"] = calc_REP(row[label])
    features_dict["ROD"] = calc_ROD(row[label])
    features_dict["label"] = label
    return features_dict

nb_rows = len(input_df.axes[0])
data = []

for index, row in input_df.iterrows():
    for label in labels:
        data.append(calc_features(row, label))
    sys.stderr.write('\rProgression: %d/%d' % (index, nb_rows))
output_df = pd.DataFrame(data)
output_df.to_csv("./dataset/dataset.csv", index=False)
print(output_df.head(5))
