from sympy.crypto.crypto import encipher_shift
from sympy.crypto.crypto import encipher_affine
from sympy.crypto.crypto import encipher_bifid5
from sympy.crypto.crypto import encipher_vigenere
from sympy.crypto.crypto import encipher_substitution
from sympy.crypto.crypto import encipher_hill

from sympy import Matrix
import sys
import pandas as pd
import random

# --- Constants ---

ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
NOJ_ALPHABET = "ABCDEFGHIKLMNOPQRSTUVWXYZ"

#--- Plaintext instances loading ---

df = pd.read_csv("./dataset/processed_plaintext.csv")
X = df.iloc[:, 0].values


#--- variables for Cyphertext instances ---

shift_cipher = []
affine_cipher = []
bifid_cipher = []
vigenere_cipher = []
substitution_cipher = []
hill_cipher = []

#--- utility functions ---

def generate_invertible_matrix():
    mat = Matrix([[random.randint(0, 9), random.randint(0, 9)],
                  [random.randint(0, 9), random.randint(0, 9)]])
    while mat.det() == 0:
        mat = Matrix([[random.randint(0, 9), random.randint(0, 9)],
                      [random.randint(0, 9), random.randint(0, 9)]])
    return mat


#--- Loop over instances ---
i = 0

for pt in X:
    i += 1

    #--- shift cipher ---
    k = random.randint(0, 25)
    shift_cipher.append(encipher_shift(pt, k))

    #--- affine cipher ---
    a = random.choice([1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25])
    b = random.randint(0, 25)
    affine_cipher.append(encipher_affine(pt, (a, b)))

    #--- bifid cipher ---
    bifid_key = ''.join(random.choice(NOJ_ALPHABET) for i in range(10))
    bifid_cipher.append(encipher_bifid5(pt.replace('J', 'I'), bifid_key))

    #--- vigenere cipher ---
    vig_key = ''.join(random.choice(ALPHABET) for i in range(10))
    vigenere_cipher.append(encipher_vigenere(pt, vig_key))

    #--- substitution cipher ---
    old = ''.join(random.choice(ALPHABET) for i in range(10))
    new = ''.join(random.choice(ALPHABET) for i in range(10))
    substitution_cipher.append(encipher_substitution(pt, old, new))

    #--- hill cipher ---
    hill_key = generate_invertible_matrix()
    hill_cipher.append(encipher_hill(pt, hill_key))

    #--- progression ---
    sys.stderr.write('\rProgression: %.3f' % round(i/len(X) * 100, 2) + '%')


#--- saving encrypted instances ---

d = {
    "Plaintext": X,
    "Shift": shift_cipher,
    "Affine": affine_cipher,
    "Bifid": bifid_cipher,
    "Vigenere": vigenere_cipher,
    "Substitution": substitution_cipher,
    "Hill": hill_cipher,
}

df = pd.DataFrame(data=d)
df.to_csv("./dataset/processed_ciphertext.csv", index=False)

