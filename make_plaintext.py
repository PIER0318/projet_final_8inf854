import pandas as pd
import re

def slice_text(text, l):
    sliced_text = []
    for i in range(0, len(text)-l, l):
        sliced_text.append(text[i:l+i])
    return sliced_text

df = pd.read_csv("./dataset/gutenberg/gutenberg_data.csv")
X = df.iloc[:, :6].values

instances = []
for i in range(len(X)):
    processed_text = re.sub(r'[^a-zA-Z]', '', X[i, -1]).upper()
    instances += slice_text(processed_text, 1000)

df2 = pd.DataFrame(instances, columns=['Plaintext'])
df2.to_csv("./dataset/processed_plaintext.csv", index=False)
